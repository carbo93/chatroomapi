
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import VueRouter from 'vue-router';
import routes from './routes';
import VueSweetalert2 from 'vue-sweetalert2';
import VueConfig from 'vue-config';
import configs from './configs';
import Vuex from 'vuex';
import store from './store';
import BootstrapVue from 'bootstrap-vue'


Vue.use(BootstrapVue);
Vue.use(VueSweetalert2);
Vue.use(VueConfig, configs);    //全域變數
Vue.use(VueRouter);
Vue.use(Vuex);
const router = new VueRouter({
    routes
});
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.component('main-app', require('./components/MainApp.vue'));
Vue.component('chat-message', require('./components/Pages/ChatMessage.vue'));

const app = new Vue({
    el: '#app',
    router,
    store,
});
