import Login from './components/Login.vue'
import Register from './components/Register.vue'
import Chat from './components/Pages/Chat.vue'
import ChatMain from './components/Pages/ChatMain.vue'
import CreateGroup from './components/Pages/CreateGroup.vue'
import CreateFriend from './components/Pages/CreateFriend.vue'
import MyInfo from './components/Pages/MyInfo.vue'
const routes = [
    { path: '/', component: Login, name: 'MainApp' },
    { path: '/login', component: Login, name: 'Login', props: true},
    { path: '/Register', component: Register, name: 'Register', props: true },
    {
        path: '/Chat', component: Chat, name: 'Chat',
        children: [
            {
                path: '/',
                redirect: 'ChatMain'
            },
            {
                path: 'ChatMain',
                component: ChatMain,
                name: 'Chat.ChatMain',
            },
            {
                path: 'CreateGroup',
                component: CreateGroup,
                name: 'Chat.CreateGroup',
            },
            {
                path: 'CreateFriend',
                component: CreateFriend,
                name: 'Chat.CreateFriend',
            },
            {
                path: 'MyInfo',
                component: MyInfo,
                name: 'Chat.MyInfo',
            },
            {
                path: '*',
                redirect: 'ChatMain'
            },

        ]
    },



    { path: '*', redirect: '/login', name: 'NoRouter'},
];

export default routes;