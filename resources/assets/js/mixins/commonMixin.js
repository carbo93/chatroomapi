/* commonMixin.js */
export default {
    methods: {
        checkUserToken(token){
            let vm = this;
            let API_URL = this.$config.API_URL;
            axios.get(API_URL + 'api/user?token=' + token)
                .then(function (response) {
                    // handle success
                    vm.$store.commit('setUserData', response.data.user);
                    if (typeof (response.data.user) == "undefined") {
                        vm.$swal('通知訊息', '請先登入您的帳號!', 'error');
                        vm.$store.dispatch('setJWTToken', '');
                        vm.$router.push('/login');
                    }
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                    vm.$swal('通知訊息', '請先登入您的帳號!', 'error');
                    vm.$store.dispatch('setJWTToken', '');
                    vm.$router.push('/login');
                })
        },
        //取得使用者資訊
        getUerInfo(token) {
            console.log('getUerInfo');
            let vm = this;
            let API_URL = vm.$config.API_URL;
            console.log(API_URL + 'api/user?token=' + token);
            axios.get(API_URL + 'api/user?token=' + token)
                .then(function (response) {
                    // handle success
                    vm.$store.commit('setUserData', response.data.user);
                    vm.getGroupList(token, response.data.user.id);
                    console.log("response.data");
                    console.log(response.data);

                    if (typeof (response.data.user) == "undefined") {
                        vm.$swal('通知訊息', '請先登入您的帳號!', 'error');
                        vm.$store.dispatch('setJWTToken', '');
                        vm.$router.push('/login');
                    }

                })
                .catch(function (error) {
                    // handle error
                    vm.$swal('通知訊息', '請先登入您的帳號!', 'error');
                    vm.$store.dispatch('setJWTToken', '');
                    vm.$router.push('/login');
                    console.log(error);

                });

        },
        getGroupList(token,uid) {    
            console.log('getGroupList');
            let vm = this;
            let API_URL = this.$config.API_URL;
            console.log(API_URL + 'api/groupsList?token=' + token + '&user_id=' + uid);
            axios.get(API_URL + 'api/groupsList?token=' + token + '&user_id=' + uid)
                .then((response) => {
                    console.log('Group');
                    console.log(response.data[0]);
                    vm.$store.commit('setGroupsData', response.data);
                    console.log("GO CHAT");
                    vm.$router.push('/Chat');
                });
        },
        logout(token) {
            let vm = this;
            let API_URL = this.$config.API_URL;
            axios.get(API_URL + 'api/logout?token=' + token)
                .then(function () {
                    vm.$swal('通知訊息', '登出成功!', 'success');
                    vm.$store.dispatch('setJWTToken', '');
                    vm.$router.push('/login');
                });

        },
    }
}