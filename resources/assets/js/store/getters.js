export default {
    count: state => state.count, //取得state裡面的內容
    getUserData(state){
        return state.userData
    },
    getGroupsData(state){
        return state.groupsData
    },
    getJWTToken(state) {
        return state.jwtToken
    },
}