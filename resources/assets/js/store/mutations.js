export default {
    addCount(state, num) {
        state.count += num //改變成state.js定義的count數值去加1，num是在actions我們丟進去的1
    },
    setUserData(state, user){
        state.userData = user
    },
    setGroupsData(state, groups) {
        state.groupsData = groups
    },
    addOneGroupData(state, group) {
        //state.groupsData.push(group)
        state.groupsData.unshift(group)
    }, 
    removeOneGroupData(state, group_id) {

        for (let i = 0; i < state.groupsData.length; i++) {
            if(state.groupsData[i].id == group_id){

                state.groupsData.splice(i, 1);
            } 
        }

    }, 
    setJWTToken(state, token) {
        state.jwtToken = token
    },
    
}