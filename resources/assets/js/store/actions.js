export default {
    addCount({ commit }) {
        commit('addCount', 1) //呼叫mutations
    },
    setUserData(state, user) {
        state.commit('setUserData', user) //呼叫mutations
    },
    setGroupsData(state, groups) {
        state.commit('setGroupsData', groups) //呼叫mutations
    },
    setJWTToken(state, token) {
        state.commit('setJWTToken', token) //呼叫mutations
    },
}