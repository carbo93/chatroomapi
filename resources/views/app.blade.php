<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="匿名交友,聊天室,交友,聊天,唱歌,交友平台,匿名聊天">
	<meta name="description" content="ㄎㄧㄤ社聊天網主打匿名聊天及匿名交友，如星座、旅遊、運動、美妝等主題">
	<meta property="og:title" content="聊天室列表 | ㄎㄧㄤ社聊天聊天網" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://wchatroom.ga" />
	<meta property="og:image" content="http://wchatroom.ga" />
	<meta property="og:description" content="ㄎㄧㄤ社聊天網主打匿名聊天及匿名交友，如星座、旅遊、運動、美妝等主題">
	<meta property="og:locale" content="zh_TW" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ㄎㄧㄤ社聊天網</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style type="text/css">
        .app{
            margin-right: auto;
            margin-left: auto;
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
</head>
<body>
    <div id="app" class='app'>
    <main-app></main-app>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
