<?php
//
//JWT參考網站
//https://tutsforweb.com/restful-api-in-laravel-56-using-jwt-authentication/
//
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');
Route::get('logout', 'ApiController@logout');

Route::group(['middleware' => 'auth.jwt'], function () {
    
    Route::get('user', 'ApiController@getAuthUser');
    Route::resource('groups', 'GroupController');
    Route::get('groupsList', 'GroupController@groupsList');
    Route::resource('conversations', 'ConversationController');
    Route::post('chatHistory', 'ConversationController@chatHistory');
    
 
    Route::get('products', 'ProductController@index');
    Route::get('products/{id}', 'ProductController@show');
    Route::post('products', 'ProductController@store');
    Route::put('products/{id}', 'ProductController@update');
    Route::delete('products/{id}', 'ProductController@destroy');



    Route::post('/friend/store', 'FriendController@store')->name('friends.store');
    Route::post('/friend/delete', 'FriendController@delete')->name('friends.delete');
    Route::get('/friend/list', 'FriendController@list')->name('friends.list');
    Route::get('/friend/search', 'FriendController@searchAll')->name('friends.searchAll');
    Route::get('/friend/searchWithoutExisting', 'FriendController@searchWithoutExisting')->name('friends.searchWithoutExisting');

    Route::post('/fileupload/singlefile', 'FileUploadController@singlefile')->name('fileupload.singlefile');
    Route::post('/fileupload/userProfile', 'FileUploadController@userProfile')->name('fileupload.userProfile');

});
