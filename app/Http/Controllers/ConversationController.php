<?php

namespace App\Http\Controllers;

use App\Models\Conversation;
use App\Models\Group;
use App\Events\NewMessage;
use Illuminate\Http\Request;

class ConversationController extends Controller
{
    public function store(Request $request)
    {
        $group_id = $request->get('group_id');
        $message = $request->get('message');
        $conversation = Conversation::create([
            'message' => $message,
            'group_id' => $group_id,
            'user_id' => auth()->user()->id,
            'avatar' => auth()->user()->avatar,
        ]);
        
        Group::Where('id',$group_id)->update(['updated_at'=>date("Y-m-d H:i:s")]);
        broadcast(new NewMessage($conversation))->toOthers();

        return $conversation->load('user');
    }


    public function chatHistory()
    {
        $conversation = Conversation::where([
            'group_id' => request('group_id')
        ])->get();

        return $conversation->load('user');
    }

}
