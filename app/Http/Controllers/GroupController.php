<?php

namespace App\Http\Controllers;

use App\Events\GroupCreated;
use App\Models\GroupUser;
use App\Models\Group;
use App\Models\Conversation;
use Illuminate\Http\Request;
use Log;
use DB;
class GroupController extends Controller
{
    public function store(Request $request)
    {

        if($request->has('name')){
            if($request->get('name')==NULL||$request->get('name')==""){
                $name = "哎呀我忘記輸入群組名稱了";
            }else{
                $name = $request->get('name');
            }
        }else{
            $name = "哎呀我忘記輸入群組名稱了";
        }
       
        if($request->hasFile('file')){
            $file = $request->file('file');
            $real_file_name = $file->getClientOriginalName();
            $filename = date("YmdHis").rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
            $ext = $file->getClientOriginalExtension();
            $filename = "group_".$filename.".".$ext;
            $destinationPath = 'uploads';
            $file->move($destinationPath,$filename);
            //Log::info('FILE GET');
        }else{
            $filename = "http://chatroomapi.example/uploads/unknown.png";
            //Log::info('FILE NOT GET');
        }
        $file_url = "http://chatroomapi.example/".$destinationPath."/".$filename;
        $group = Group::create(['name' => $name,'image' => $file_url]);

        $users = explode(',',request('users'));

        array_push($users, request('user_id'));

        $group->users()->attach($users);

        broadcast(new GroupCreated($group))->toOthers();

        return $group;
    }
    public function groupsList(Request $request)
    {

        if($request->has('user_id')){
            $user_id = $request->get('user_id');
        }else{
            $user_id = "";
        }
        
        $groups = DB::select('select * from group_user as a, groups as b where a.group_id = b.id and a.user_id=? order by b.updated_at desc',[$user_id]);
        
        foreach($groups as $group){
            //dd($group);
            $GUserCount = GroupUser::where('group_id', '=', $group->group_id)->count();
            //dd($GUserCount);
            $group->UserCount = $GUserCount;
            $Conversation = Conversation::where('group_id', '=', $group->group_id)->orderBy('id', 'desc')->first();
            if(isset($Conversation->message)){
                if(strstr($Conversation->message,"href")){
                   $group->Conversation = "貼圖已傳送";
                }else{
                   $group->Conversation = $Conversation->message;
                }
            }else{
                $group->Conversation = '';
            }
        }
        
        return json_encode($groups);
    }
}
