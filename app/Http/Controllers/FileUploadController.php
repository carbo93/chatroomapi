<?php

namespace App\Http\Controllers;
use App\Models\Conversation;
use App\User;
use App\Events\NewMessage;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    public function singlefile(Request $request)
    {
      $file = $request->file('file');
      $real_file_name = $file->getClientOriginalName();
      $filename = date("YmdHis").rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
      $ext = $file->getClientOriginalExtension();
      $filename = $filename.".".$ext;
      /*
      //Display File Real Path
      echo 'File Real Path: '.$file->getRealPath();
      echo '<br>';
   
      //Display File Size
      echo 'File Size: '.$file->getSize();
      echo '<br>';
   
      //Display File Mime Type
      echo 'File Mime Type: '.$file->getMimeType();
      */
      //Move Uploaded File
      $destinationPath = 'uploads';
      $file->move($destinationPath,$filename);
      
      switch(strtolower($ext)){
        case "gif":
        case "jpg":
        case "jpeg":
        case "png":
            $file_link = "<a href='http://chatroomapi.example/".$destinationPath."/".$filename."' target='blank'><img src='../".$destinationPath."/".$filename."' width='100px'/></a>";
        break;
        default:
             $file_link = "<a href='http://chatroomapi.example/".$destinationPath."/".$filename."' target='blank'>".$real_file_name."</a>";
        break;
      }
    
      $conversation = Conversation::create([
            'message' => $file_link,
            'group_id' => request('group_id'),
            'user_id' => auth()->user()->id,
            'avatar' => auth()->user()->avatar,
      ]);

      broadcast(new NewMessage($conversation))->toOthers();

        return [
            'message' => $file_link,
            'user' => [
                'id' => auth()->user()->id,
                'name' => auth()->user()->name,
                'avatar' => auth()->user()->avatar,
            ]
        ];

    
    }

    public function userProfile(Request $request)
    {
      $user_id = $request->get('user_id');
      $desc = $request->get('desc');
      $name = $request->get('name');
      $zodiac = $request->get('zodiac');
      $birthdate = $request->get('birthdate');
      $phone = $request->get('phone');
      $mobile = $request->get('mobile');
      $address = $request->get('address');
      $facebook = $request->get('facebook');
      $website = $request->get('website');
      $instagram = $request->get('instagram');

      if($request->hasFile('file')){
        $file = $request->file('file');
        $real_file_name = $file->getClientOriginalName();
        $filename = date("YmdHis").rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $ext = $file->getClientOriginalExtension();
        $filename = $filename.".".$ext;
        $destinationPath = 'uploads/avatar/';
        $file->move($destinationPath,$filename);
        $link = "http://chatroomapi.example/".$destinationPath.$filename;
        $updateDATA = [
              'avatar' => $link,
              'desc' => $desc,
              'name' => $name,
              'zodiac' => $zodiac,
              'birthdate' => $birthdate,
              'phone' => $phone,
              'mobile' => $mobile,
              'address' => $address,
              'instagram' => $instagram,
              'facebook' => $facebook,
              'website' => $website,
              'updated_at' => date("Y-m-d H:i:s")
        ];
      }else{
          $updateDATA = [
              'desc' => $desc,
              'name' => $name,
              'zodiac' => $zodiac,
              'birthdate' => $birthdate,
              'phone' => $phone,
              'mobile' => $mobile,
              'address' => $address,
              'instagram' => $instagram,
              'facebook' => $facebook,
              'website' => $website,
              'updated_at' => date("Y-m-d H:i:s")
          ];
      }
      $affectedRows = User::where('id', '=', $user_id)
      ->update(
          $updateDATA
        );

      if($affectedRows){
        if($request->hasFile('file')){
            $back = ['result'=>'success', 'link'=>$link];
            return json_encode($back);
        }else{
            $back = ['result'=>'success', 'link'=>$link];
            return json_encode($back);
        }
      }else{
          $back = ['result'=>'fail', 'link'=>''];
          return json_encode($back);
      }
      
    }

}
