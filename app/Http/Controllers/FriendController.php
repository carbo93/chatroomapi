<?php

namespace App\Http\Controllers;

use App\Events\GroupCreated;
use App\Models\Friend;
use App\User;
use Illuminate\Http\Request;

class FriendController extends Controller
{
    public function store(Request $request)
    {
        $user_id = request('user_id');
        $friend_id = request('friend_id');
        $friend = Friend::create(['user_id' => $user_id ,'friend_id' => $friend_id]);
        $friend = User::where('id', $friend_id)->first();

        return $friend;
    
    }

    public function delete(Request $request)
    {

        $user_id = request('user_id');

        $friend_id = request('friend_id');

        $friend = Friend::where('user_id', $user_id)->where('friend_id',$friend_id)->delete();

        return $friend;
    
    }

    public function list(Request $request)
    {
        
        if(!$request->has('user_id')){
            return 'fail';
        }

        $user_id = $request->get('user_id');

        $friends = Friend::where('user_id', $user_id)->get();
        $whereIn = array();
        foreach($friends as $fd){
            array_push($whereIn ,$fd->friend_id);
        }
 
        $friends = User::whereIn('id', $whereIn)->where('id','!=',$user_id)->get();
       
        return json_encode($friends);


    }

    public function searchAll(Request $request)
    {
        $friend_name = request('friend_name');
        $user_id = request('user_id');
        
        $friends = User::where('name', 'like', $friend_name.'%')->get();

        return json_encode($friends);
    }

    public function searchWithoutExisting(Request $request)
    {
        $friend_name = request('friend_name');
        $user_id = request('user_id');
 
        $friends = Friend::where('user_id', $user_id)->get();
        $whereIn = array();
        foreach($friends as $fd){
            array_push($whereIn ,$fd->friend_id);
        }
        if(count($whereIn)==0){
          $friends = User::where('name', 'like', $friend_name.'%')->where('id','!=',$user_id)->get();
        }else{
          $friends = User::where('name', 'like', $friend_name.'%')->where('id','!=',$user_id)->whereNotIn('id', $whereIn)->get();
        }

        if(count($friends)>0){
            return json_encode($friends);
        }else{
            return 'fail';
        }
    }
    

}
